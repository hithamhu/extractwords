package com.hithamhl.textparse.ui;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hithamhl.textparse.R;
import com.hithamhl.textparse.adapter.WordAdapter;
import com.hithamhl.textparse.model.Word;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class WordFragment extends DialogFragment {

    RecyclerView wordRecycleView;
    WordAdapter wordAdapter;
    ArrayList<Word> wordArrayList;


    public WordFragment() {
        // Required empty public constructor
    }

    public static WordFragment newInstance(ArrayList<Word> words){
        WordFragment wordFragment=new WordFragment();
        Bundle args=new Bundle();
        args.putParcelableArrayList("WordList",words);
        wordFragment.setArguments(args);

        return wordFragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       return inflater.inflate(R.layout.fragment_word, container, true);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        wordRecycleView=view.findViewById(R.id.word_recycle);
        wordArrayList =new ArrayList<>();
        wordArrayList=getArguments().getParcelableArrayList("WordList");

        wordAdapter=new WordAdapter(getContext(), wordArrayList, new WordAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        });
        wordRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        wordRecycleView.setAdapter(wordAdapter);


    }
    @Override
    public void onResume() {
        // Get existing layout params for the window
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(params);
        // Call super onResume after sizing
        super.onResume();
    }



}

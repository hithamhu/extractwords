package com.hithamhl.textparse.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import com.hithamhl.textparse.R;
import com.hithamhl.textparse.databinding.ActivityMainBinding;
import com.hithamhl.textparse.model.Word;
import com.hithamhl.textparse.utils.ToolsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    public static final String PARAGRAPH="para_graph";
    int mix_frqunts=0;
    ActivityMainBinding activityMainBinding;
    ArrayList<Word>wordArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.runBtn.setOnClickListener(this);
        activityMainBinding.pastBtn.setOnClickListener(this);


        if (savedInstanceState!=null){
            activityMainBinding.paragraphTxt.setText(savedInstanceState.getString(PARAGRAPH));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PARAGRAPH,getParagraph());
    }

    private String getParagraph() {
        return activityMainBinding.paragraphTxt.getText().toString();
    }

    private void do_work(){
        Log.d(TAG, "do_work:Paragraph "+getParagraph());
        String paragraph=getParagraph();
        if(paragraph.equals("")){
            Toast.makeText(this, "Please write paragraph", Toast.LENGTH_SHORT).show();
        }else{
            selectWord();

        }
    }

    private void pasetTextToPlainText(){
        String text=ToolsUtils.pasteText(getApplicationContext());
        if(text!=null){
            activityMainBinding.paragraphTxt.setText(text);
        }else {
            Toast.makeText(this, "Please paste some Text :)", Toast.LENGTH_SHORT).show();
        }
    }

   private void selectWord(){
        ArrayList<String> excludeWord;
        excludeWord=ToolsUtils.wordExclude();
       Log.d(TAG, "selectWord:arraySize "+excludeWord.size());
        String text=getParagraph();
       HashMap<String,Integer> wordFreq=new HashMap<>();
       String[] textSpilit=text.split("[:,. ]");
       for (String word:textSpilit){
           String Key=word.toLowerCase().trim();

           if(Key.length()==0) {continue;}
           //to remove exclude word
           if(excludeWord.contains(Key)){continue;}
           if(!wordFreq.containsKey(Key)){
               wordFreq.put(Key,1);
           }else {
               int oldFreq=wordFreq.get(Key);
               wordFreq.put(Key,oldFreq+1);
           }

       }


       ////////////////////////////////////////////final result///////////////
       String final_text="";
       wordArrayList=new ArrayList<>();
       for (Map.Entry<String, Integer> selectWord: wordFreq.entrySet()){
           String key_select=selectWord.getKey();
           int freq=selectWord.getValue();
           Log.d(TAG, "selectWord:hash "+wordFreq.size());
           wordArrayList.add(new Word(key_select,freq));
          // final_text=final_text.concat("Word: "+key_select+" freq: "+freq+"\n");

       }

       showWordDialog(wordArrayList);



      // activityMainBinding.paragraphTxt.setText(final_text);

   }

    private void showWordDialog(ArrayList<Word>wordsList) {
        FragmentManager fm = getSupportFragmentManager();
        WordFragment wordFragment=WordFragment.newInstance(wordsList);
        wordFragment.show(fm,"words");
    }


    @Override
    public void onClick(View v) {
       if (v.getId()==activityMainBinding.runBtn.getId()){
           do_work();
       }
       else if (v.getId()==activityMainBinding.pastBtn.getId()){
           pasetTextToPlainText();
       }


    }


}

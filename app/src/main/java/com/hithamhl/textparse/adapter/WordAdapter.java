package com.hithamhl.textparse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.hithamhl.textparse.R;
import com.hithamhl.textparse.model.Word;

import java.util.List;

public class WordAdapter extends

        RecyclerView.Adapter<WordAdapter.ViewHolder> {

    private static final String TAG = WordAdapter.class.getSimpleName();

    private Context context;

    private List<Word> list;

    private OnItemClickListener onItemClickListener;

    public WordAdapter(Context context, List<Word> list,

                       OnItemClickListener onItemClickListener) {

        this.context = context;

        this.list = list;

        this.onItemClickListener = onItemClickListener;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView word,fraq;

        public ViewHolder(View itemView) {

            super(itemView);
            word=itemView.findViewById(R.id.row_word);
            fraq=itemView.findViewById(R.id.row_freq);
        }

        public void bind(final Word model,

                         final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {

                    listener.onItemClick(getLayoutPosition());

                }

            });

        }

    }

    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.word_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;

    }

    @Override

    public void onBindViewHolder(ViewHolder holder, int position) {

        Word item = list.get(position);

        holder.bind(item, onItemClickListener);
        holder.word.setText(item.getWord());
        holder.fraq.setText(item.getFreq()+" ");

    }

    @Override

    public int getItemCount() {

        return list.size();

    }

    public interface OnItemClickListener {

        void onItemClick(int position);

    }

}
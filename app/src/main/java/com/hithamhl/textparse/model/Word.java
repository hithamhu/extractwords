package com.hithamhl.textparse.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Word implements Parcelable {

    String word;
    int freq;

    public Word(String word, int freq) {
        this.word = word;
        this.freq = freq;
    }

    protected Word(Parcel in) {
        word = in.readString();
        freq = in.readInt();
    }

    public static final Creator<Word> CREATOR = new Creator<Word>() {
        @Override
        public Word createFromParcel(Parcel in) {
            return new Word(in);
        }

        @Override
        public Word[] newArray(int size) {
            return new Word[size];
        }
    };

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getFreq() {
        return freq;
    }

    public void setFreq(int freq) {
        this.freq = freq;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(word);
        dest.writeInt(freq);
    }
}

package com.hithamhl.textparse.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ToolsUtils {
  static   ArrayList<String> wordToExclude;

    public static String pasteText(Context context){
        ClipboardManager clipboardManager=(ClipboardManager)context.getSystemService(Context.CLIPBOARD_SERVICE);
        assert clipboardManager != null;
        ClipData.Item item= clipboardManager.getPrimaryClip().getItemAt(0);
        String pasetItem=item.getText().toString();


        return item.getText().toString();
    }


    public static ArrayList<String> wordExclude(){
        //word should not include in process
        wordToExclude=new ArrayList<>();
        wordToExclude.add("a");
        wordToExclude.add("he");
        wordToExclude.add("she");
        wordToExclude.add("the");
        wordToExclude.add("of");
        wordToExclude.add("in");
        wordToExclude.add("on");
        wordToExclude.add("to");
        wordToExclude.add("at");
        wordToExclude.add("is");
        wordToExclude.add(".");
        wordToExclude.add(",");
        wordToExclude.add("or");
        return wordToExclude;
    }
}
